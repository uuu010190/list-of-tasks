const form = document.querySelector("#new-task-form");
const input = document.querySelector("#new-task-input");
const listOfElements = document.querySelector("#tasks");

window.addEventListener("load", ()=>{
	listOfTasks = JSON.parse(localStorage.getItem("listOfTasks")) || [];

	form.addEventListener("submit", (e)=>{
		e.preventDefault();

		const task = input.value;
		if(task === ""){
			alert("Enter a task");
			return;
		}

		listOfTasks.push(task);

		localStorage.setItem("listOfTasks", JSON.stringify(listOfTasks));

		displayTasks(listOfTasks);

		e.target.reset();
	});

	displayTasks(listOfTasks);
});

function displayTasks(listOfTasks){
	listOfElements.innerHTML = "";

	listOfTasks.forEach(taskItem => {
		const taskContainer = document.createElement("div");
		taskContainer.classList.add("task");

		const taskContentContainer = document.createElement("div");
		taskContentContainer.classList.add("content");

		taskContainer.appendChild(taskContentContainer);

		const taskContent = document.createElement("input");
		taskContent.classList.add("text");
		taskContent.setAttribute("id", `text-${listOfTasks.indexOf(taskItem)}`);
		taskContent.type = "text";
		taskContent.value = taskItem;
		taskContent.setAttribute("readonly", "readonly");

		taskContentContainer.appendChild(taskContent);

		const taskActionsContainer = document.createElement("div");
		taskActionsContainer.classList.add("actions");

		const taskEditButton = document.createElement("button");
		taskEditButton.classList.add("edit");
		taskEditButton.innerText = "Edit";

		const taskDeleteButton = document.createElement("button");
		taskDeleteButton.classList.add("delete");
		taskDeleteButton.innerText = "Delete";

		taskActionsContainer.appendChild(taskEditButton);
		taskActionsContainer.appendChild(taskDeleteButton);

		taskContainer.appendChild(taskActionsContainer);

		listOfElements.appendChild(taskContainer);

		editTask(taskEditButton, taskContent, taskItem);

		deleteTask(taskDeleteButton, listOfElements, taskContainer,taskItem);
	});
}

function editTask(taskEditButton, taskContent, taskItem){
	taskEditButton.addEventListener("click", () =>{
		if(taskEditButton.innerText.toLowerCase() === "edit"){
			taskEditButton.innerText = "Save";
			taskContent.removeAttribute("readonly");
			taskContent.focus();
		}else{
			listOfTasks[listOfTasks.indexOf(taskItem)] = document.querySelector(`.content #text-${listOfTasks.indexOf(taskItem)}`).value;
			localStorage.setItem("listOfTasks", JSON.stringify(listOfTasks));
			listOfTasks = JSON.parse(localStorage.getItem("listOfTasks")) || [];
			displayTasks(listOfTasks);
			taskEditButton.innerText = "Edit";
			taskContent.setAttribute("readonly", "readonly");
		}
	});
}

function deleteTask(taskDeleteButton, listOfElements, taskContainer,taskItem){
	taskDeleteButton.addEventListener("click", ()=>{
		listOfTasks.splice(listOfTasks.indexOf(taskItem), 1);
		localStorage.setItem("listOfTasks", JSON.stringify(listOfTasks));
		listOfTasks = JSON.parse(localStorage.getItem("listOfTasks")) || [];
		displayTasks(listOfTasks);
	});
}